# Zancudómetro

El Zancudómetro es un dispositivo electrónico que
cuenta la cantidad de mosquitos atrapados en una 
trampa electŕonica de mosquitos de descarga electroestática.

Este repositorio contiene el código fuente e 
instrucciones de armado del proyecto denominado 
"Zancudómetro" desarrollado como parte de los 
Laboratorios de Resiliencia Comunitarios.

Prototipo elaborado por: Jocksan Alvarado @ Hackerspace San Salvador

## Lista de Materiales
Para construir este proyecto necesitas los siguientes materiales:

| Nombre                          | Cantidad |
|---------------------------------|----------|
| Arduino UNO R3                  |        1 |
| Trampa eléctrica caza-mosquitos |        1 |
| Diodo tipo Schottky             |        2 |
| Resistencia de 10K              |        3 |
| Breadboard                      |        1 |
| Cable Jumper M/M                |        8 |
| Alambre aislado                 |     20cm |

## Diagrama de Conexión
![](./Docu/Esquematico_bb.png)

## Guía de Conexion en Breadboard
Las conexiónes que comienzan con **BB** se conectan en 
la Breadboard. Las que comienzan con **AR** se conectan 
al Arduino.

| Componente                         |        |         |
|------------------------------------|--------|---------|
| Jumper 1                           | BB-J1  | BB-V+   |
| Resistencia 1                      | BB-I1  | BB-I5   |
| Resistencia 2                      | BB-H5  | BB-H9   |
| Jumper 2                           | BB-J9  | BB-V-   |
| Jumper 3                           | BB-F5  | AR-13   |
| Resistencia 2                      | BB-I15 | BB-I19  |
| Diodo Schotkky 1                   | BB-H19 | BB-H15  |
| Diodo Schotkky 2                   | BB-G15 | BB-G19  |
| Jumper 4                           | BB-F15 | AR-12   |
| Jumper 5                           | BB-F19 | BB-V-   |
| Jumper 6                           | BB-V+  | AR-5V   |
| Jumper 7 (Conectar de lado a lado) | BB-V+  | BB-V+   |
| Jumper 8 (Conectar de lado a lado) | BB-V-  | BB-V-   |
| Jumper 9                           | BB-V-  | AR-GND  |

## Información para Colaborar con este Proyecto
Para colaborar con este proyecto solicita acceso al canal
oficial de Slack en [https://labsresiliencia.slack.com](https://labsresiliencia.slack.com) 
y únete al canal ***#propuesta_b5***.
