volatile int pulsos = 0;
int cuenta = 0;

void setup() {
 Serial.begin(9600);
 pinMode(13, OUTPUT);
 
 ADCSRB = 0;           // (Disable) ACME: Analog Comparator Multiplexer Enable
 ACSR =  bit (ACI)     // (Clear) Analog Comparator Interrupt Flag
         | bit (ACIE)    // Analog Comparator Interrupt Enable
         | bit (ACIS1)
         | bit (ACIS0);  // ACIS1, ACIS0: Analog Comparator Interrupt Mode Select (trigger on rising edge)
}

void loop() {
  cli();
  while (pulsos > 0) {
    pulsos--;
    cuenta++;
    Serial.println(cuenta);
  }
  sei();
}

ISR(ANALOG_COMP_vect) {
  pulsos++;
  digitalWrite(13, !digitalRead(13));
}
